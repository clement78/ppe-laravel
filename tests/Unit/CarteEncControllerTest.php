<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabaseState ;

class CarteEncControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return
     *
     * void
     */

    use RefreshDatabase ;
    public function testMain()
    {
        /*$response = $this->GET('demandecarte');
        $response->assertRedirect(route('demandeCarte.index'));

        $response = $this->GET('demandeCarte');
        $response->assertRedirect(route('demandeCarte.create'));*/


        $response = $this->post('register',[
            'name' => 'Billien',
            'email' => 'clement@hotmail.com',
            'password' => 'test2020',
            'password_confirmation' => 'test2020',
        ]);
        /* assertRedirect permet de faire une rediréction de notre page d'enregistrement à la page d'ashboard*/
        $response->assertRedirect(route('dashboard')) ;
        /* assertDatabaseHas permet de vérifier que l'adresse indiqué est dans notre base de donné*/
        $this->assertDatabaseHas('Users',['email' => 'clement@hotmail.com']) ;


        // Si le CDP impose l'existence d'un fichier pour pouvoir faire l'enregistrement
        // je dois uploader ici un fichier pour faire le test

        $response = $this->post('demandeCarte', [
            'nomEtudiantFormulaire' => 'Clément',
            'email' => 'clement@hotmail.com',
            'numeroTelephoneFormulaire' => '0777445566',
            'dateEntreeENC' => '2021-04-28',
            'section' => 'SLAM',
        ]);
        /* Avec le assertRedirect on test la rediréction de notre page à la page demandeCarte.index */
        $response->assertRedirect(route('demandeCarte.index'));
        /* On test que notre email crée est bien dans la table carte_etudiants de la base de donnée */
        $this->assertDatabaseHas('carte_etudiants', ['email' => 'clement@hotmail.com']);

        /*On recupère avec get la page demandeCarte*/
        $response = $this->get('demandeCarte');
        /* On test le retour HTTPS de notre page demandeCarte avec AssertStatus*/
        $response->assertStatus(200) ;

/* Cette méthode permet de vérifier la modification d'une donnée de notre carte comme par exemple un email*/
        /*On crée une demande de carte en spécifiant l'id de la carte dans la BDD*/
        $response = $this->PUT('demandeCarte/1', [
            'nom' => 'Clément',
            'email' => 'clementnouveau@hotmail.com',
            'number' => '0744445566',
        ]);
        /* On fait un assertRedirect sur la route demandeCarte.index pour avec une rediréciton sur cette page après la modification de la carte*/
        $response->assertRedirect(route('demandeCarte.index'));
        /* On fait un assertDatabaseHase pour vérifier notre nouvel adresse mail dans la BDD */
        $this->assertDatabaseHas('carte_etudiants', ['email' => 'clementnouveau@hotmail.com']);

/* On test la methode delete qui supprime une carte */
        /* On indique l'id de la carte a supprimer */
        $response = $this->DELETE('demandeCarte/1');
/* On test la redirection donc le retour de notre page sur demandeCarte.index */
        $response->assertRedirect(route('demandeCarte.index'));
        /* On test avec assertDatabaseMissing qu'il n'y a plus les données de la carte comme l'email dans le BDD */
        $this->assertDatabaseMissing('carte_etudiants', ['email' => 'clementnouveau@hotmail.com']);

    }}

